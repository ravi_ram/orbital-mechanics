"""

   Orbital Mechanics-Curtis Example_11_03
   ----------------------------------------------------
	The rocket in Example 11.2 has a diameter of 5m. It is to be 
    launched on a gravity turn trajectory. Pitchover begins at an 
    altitude of 130 m with an initial flight path angle gamma-o of 89.85 deg. 
    What are the altitude h and speed v of the rocket at burnout (tbo=260 s)? 
    What are the velocity losses due to drag and gravity)?

	Example 11.2
	The data for a single-stage rocket are as follows:
    Launch mass: m_o = 68,000 kg
    Mass ratio: n = 15
    Specific impulse: Isp = 390 
    Thrust: T = 933.91 kN
    
    g_o = 9.81 m/s^2
    
    m = m_o - m_e * t
    m_e = T / (Isp * g_o) ;T=The thrust T=933.913 kN is assumed constant 

	The drag force D = 1/2 * rho * v^2 * A * Cd
    The drag coefficient is assumed to have the constant value Cd = 0.5. 
    The frontal area A = pi * d^2 /4 is found from the rocket diameter d = 5 m. 
    
    The atmospheric density profile is assumed exponential, r = r_o * e ^-(h/h_o) 
    
    r_o = 1.225 kg/m 3 is the sea level atmospheric density and 
    h_o = 7.5 km is the scale height of the atmosphere.
    
    acceleration of gravity varies with altitude h according to Eqn (1.36),
	
    g = g_o / (1 + h_e/Re)^2
	Re = 6378 km;g_o = 9.81m/s^2

 Author: Ravi
"""

from numpy import array, zeros, linalg
from numpy import amax, where
import math, sys

from numpy import linspace
#from scipy.integrate import odeint as ODEint

from run_kut5 import *


deg    = math.pi/180;   # ...Convert degrees to radians
g0     = 9.81;          # ...Sea-level acceleration of gravity (m/s)
Re     = 6378e3;        # ...Radius of the earth (m)
hscale = 7.5e3;         # ...Density scale height (m)
rho0   = 1.225;         # ...Sea level density of atmosphere (kg/m^3)

diam   = 196.85/12*0.3048; # ...Vehicle diameter (m)

A      = math.pi/4*(diam)**2; # ...Frontal area (m^2)
CD     = 0.5;           # ...Drag coefficient (assumed constant)
m0     = 149912*.4536;  # ...Lift-off mass (kg)
n      = 15;            # ...Mass ratio
T2W    = 1.4;           # ...Thrust to weight ratio
Isp    = 390;           # ...Specific impulse (s)

mfinal = m0/n;          # ...Burnout mass (kg)
Thrust = T2W*m0*g0;     # ...Rocket thrust (N)
m_dot  = Thrust/Isp/g0; # ...Propellant mass flow rate (kg/s)
mprop  = m0 - mfinal;   # ...Propellant mass (kg)
tburn  = mprop/m_dot;   # ...Burn time (s)
hturn  = 130;           # ...Height at which pitchover begins (m)
t0     = 0;             # ...Initial time for the numerical integration
tf     = tburn;         # ...Final time for the numerical integration
tspan  = [t0,tf];       # ...Range of integration

# ...Initial conditions:
v0     = 0;             # ...Initial velocity (m/s)
gamma0 = 89.85*deg;     # ...Initial flight path angle (rad)
x0     = 0;             # ...Initial downrange distance (km)
h0     = 0;             # ...Initial altitude (km)
vD0	   = 0;             # ...Initial value of velocity loss due
                        #    to drag (m/s)
vG0    = 0;             # ...Initial value of velocity loss due
                        #    to gravity (m/s)

# ...Initial conditions vector:
f0 = [v0, gamma0, x0, h0, vD0, vG0];

# Calculates the time rates df/dt of the variables f(t) 
# in the equations of motion of a gravity turn trajectory.
# dydt = rates(t,y)
#
def func_ODE(t,y):
    #print(y);
    #...Initialize dfdt as a column vector:
    dydt = zeros(6);

    v     = y[0]; # ...Velocity
    gamma = y[1]; # ...Flight path angle
    x     = y[2]; # ...Downrange distance
    h     = y[3]; # ...Altitude
    vD    = y[4]; # ...Velocity loss due to drag
    vG    = y[5]; # ...Velocity loss due to gravity 

    #print("time = ", t);
        
    #...When time t exceeds the burn time, set the thrust
    #   and the mass flow rate equal to zero:

    if (t < tburn):
        m = m0 - m_dot*t;     # ...Current vehicle mass
        T = Thrust;           # ...Current thrust
    else:
        m = m0 - m_dot*tburn; # ...Current vehicle mass
        T = 0;                # ...Current thrust

    #print(t,m);

    g   = g0/(1 + h/Re)**2;               # ...Gravitational variation
                                          #    with altitude h
    rho = rho0 * math.exp(-h/hscale);     # ...Exponential density variation
                                          #    with altitude
    D   = 0.5 * rho * v**2 * A * CD;      # ...Drag [Equation 11.1]
    
    #print(A, rho, v**2, D);
    #print(g, rho, D);

    #...Define the first derivatives of v, gamma, x, h, vD and vG
    #   ("dot" means time derivative):
    #v_dot = T/m - D/m - g*sin(gamma); % ...Equation 11.6

    #...Start the gravity turn when h = hturn:
    if (h <= hturn):
        gamma_dot = 0;
        v_dot     = T/m - D/m - g;
        x_dot     = 0;
        h_dot     = v;
        vG_dot    = -g;
    else:
        v_dot = T/m - D/m - g*math.sin(gamma);
        gamma_dot = -1/v*(g - v**2/(Re + h))*math.cos(gamma);# ...Equation 11.7
        x_dot    = Re/(Re + h)*v*math.cos(gamma);           # ...Equation 11.8(1)
        h_dot    = v*math.sin(gamma);                       # ...Equation 11.8(2)
        vG_dot	 = -g*math.sin(gamma);                      # ...Gravity loss rate
    
    #print(v_dot, gamma_dot, x_dot, h_dot, vG_dot);    
    # ...Drag loss rate
    vD_dot   = -D/m;
    
    #...Load the first derivatives of f(t) into the vector dydt:
    dydt[0]  = v_dot;
    dydt[1]  = gamma_dot;
    dydt[2]  = x_dot;
    dydt[3]  = h_dot;
    dydt[4]  = vD_dot;
    dydt[5]  = vG_dot;  
    
    #print (dydt);
    return dydt

def run():
    # # We generate a solution (n+1) evenly spaced samples in the interval
    # # t0 <= `t` <= tf.  So our array of times is:
    # sim_time = 5
    # times = linspace(t0, tf, sim_time+1)
    # 
    # # Integrate
    # X = ODEint(func_ODE, f0, times)
    # result(X, times)


    # t0     =  Initial time for the numerical integration
    # tf     =  Final time for the numerical integration    
    # f0     =  Initial conditions vector:
    
    h    = (tf - t0)/100; # Assumed initial time step.
    #h = 0.5     # initial increment of x used in integration

    t,f = integrate(func_ODE, t0, f0, tf, h)

    # ...Solution f(t) returned on the time interval [t0 tf]: 
    # slice each eolumn into the respective arrays
    v      =  f[:,0] * 1.e-3;  # ...Velocity (km/s)
    gamma  =  f[:,1] / deg;    # ...Flight path angle (degrees)
    x      =  f[:,2] * 1.e-3;  # ...Downrange distance (km)
    h      =  f[:,3] * 1.e-3;  # ...Altitude (km)
    vD     = -f[:,4] * 1.e-3;  # ...Velocity loss due to drag (km/s)
    vG     = -f[:,5] * 1.e-3;  # ...Velocity loss due to gravity (km/s)
    
    # print( h );
    size = len(f[-1]);
    q = zeros(size);
    
    for i in range(1,size):
        Rho  = rho0 * math.exp(-h[i]*1000/hscale); #...Air density
        q[i] = 0.5 * Rho* (v[i])**2;                #...Dynamic pressure  

    # Find maxQ
    qmax = amax(q);
    # Get the indices of maximum element in numpy array
    max_index = where(q == amax(q));
    # altitude at the max q
    hqmax = h[max_index];
    #print("Max Q = %9.5f at altitude h = %9.5f " % (qmax, hqmax) );
    
    output = True;
    
    if(output):
        print("\n\n -----------------------------------\n")
        print("Initial flight path angle = %9.2f deg" % (gamma0/deg))
        print("Pitchover altitude        = %9.2f m" % hturn)
        print("Burn time                 = %9.2f s" % tburn)
        print("Final speed               = %9.2f km/s" % v[-1])
        print("Final flight path angle   = %9.2f deg " % gamma[-1])
        print("Altitude                  = %9.2f km  " % h[-1])
        print("Downrange distance        = %9.2f km " % x[-1])
        print("Drag loss                 = %9.2f km/s" % vD[-1])
        print("Gravity loss              = %9.2f km/s" % vG[-1])
        print("\n\n -----------------------------------\n")    
        
        import matplotlib.pyplot as plt
        plt.plot(x, h, 'o-', x, v, '^-', markersize=1)
        plt.grid(True)
        plt.xlabel('Distance'); plt.ylabel('Altitude, Velocity')
        plt.legend(('Altitude', 'Velocity'), loc=0)
        plt.show()

    return    
    
if __name__ == "__main__":
    run()    
	