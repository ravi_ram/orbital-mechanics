"""

   Orbital Mechanics-Curtis-Example 2.3
   ------------------------------------------------------------------------
   Relative to a nonrotating frame of reference with origin at the center 
   of the earth,  a 1000 kg satellite's initial position vector is 
   r = 8000i + 6000k  km and its initial velocity vector is v = 7j km/s.
   Solve for the path of the spacecraft over the next 4 h. 
   Determine its minimum and maximum distance from the earth's surface 
   during that time.
   
   The minimum altitude is 3622 km; and the speed at that point is 7 km/s:
   The maximum altitude is 9560 km; and the speed at that point is 4:39 km/s: 

 Author: Ravi
 """


import math
import sys
from numpy import array, zeros, linalg


"""
	This function calculates the acceleration vector using Equation 2.22

	t          - time
	f          - column vector containing the position vector and the
	           velocity vector at time t
	x, y, z    - components of the position vector r
	r          - the magnitude of the the position vector
	vx, vy, vz - components of the velocity vector v
	ax, ay, az - components of the acceleration vector a
	dydt       - column vector containing the velocity and acceleration
	           components
"""

def equations(trv):
	x  = trv[0]; y  = trv[1]; z  = trv[2];
	vx = trv[3]; vy = trv[4]; vz = trv[5];

	r = linalg.norm([x,y,z])
	k = mu / (r**3)

	ax = k*x
	ay = k*y
	az = k*z
	flow = array([ vx, vy, vz, ax, ay, az])
	return flow
 
def RK4_step(x, dt, flow):    # replaces x(t) by x(t + dt)
    k1 = dt * flow(x);     
    x_temp = x + k1 / 2;   k2 = dt * flow(x_temp)
    x_temp = x + k2 / 2;   k3 = dt * flow(x_temp)
    x_temp = x + k3    ;   k4 = dt * flow(x_temp)
    x += (k1 + 2*k2 + 2*k3 + k4) / 6
    return

def integrate( trv0, dt, F, t_max ):
    trv = trv0.copy()
    step = 0
    t = 0
    print "Integrating ... "
    while True:
		RK4_step(trv, dt, equations)
		step += 1
		t+=dt

		# Copy and append pos-velo data
		#F[:,step] = trv[:]

		# Min-Max Calculations
		d = calculate(trv)
		if(hasCollided(d)):
			print "Satellite collided with earth."
			break

		if t > t_max:
			break
    return trv

# Utility Functions

def calculate(trv):
	global max_dist, min_dist, velo_at_max, velo_at_min
	global angle_at_max, angle_at_min

	# math.atan2(y, x) returns angle (in radius) from the X -Axis 
	# to the specified point (y, x)
	angle = math.atan2(trv[1], trv[0]) * 180 / math.pi;
	#if (angle < 0.0): 
	#	angle += 360

	#dist = math.sqrt ( (trv[0] * trv[0]) + (trv[1] * trv[1]) + (trv[2] * trv[2]) )
	#velo = math.sqrt ( (trv[3] * trv[3]) + (trv[4] * trv[4]) + (trv[5] * trv[5]) )
	dist = linalg.norm([trv[0], trv[1], trv[2]])
	velo = linalg.norm([trv[3], trv[4], trv[5]])

	if(max_dist <= dist ):
		max_dist = dist
		velo_at_max = velo
		angle_at_max = angle

	if(min_dist >= dist):
		min_dist = dist
		velo_at_min = velo
		angle_at_min = angle

	return dist

def hasCollided(dist):
	global R
	if(dist > R):
		return False
	else:
		return True

def result():
	global max_dist, min_dist, velo_at_max, velo_at_min
	global angle_at_max, angle_at_min

	#print 'n=%6d, xpos=%6.2f, ypos=%6.2f, zpos=%6.2f' % \
	#		(n, trv[0], trv[1], trv[2])
	print ""
	print "Max Dist : "
	print "-------------------------------------------"
	print 'angle=%6d, dist=%6.2f velo=%6.2f' % \
			(angle_at_max, max_dist-R, velo_at_max)

	print ""
	print "-------------------------------------------"
	print "Min Dist : "
	print 'angle=%6d, dist=%6.2f velo=%6.2f' % \
			(angle_at_min, min_dist-R, velo_at_min)			
	return


# ---------------- Input Data ----------------
G 	= 6.6742E-20			# Gravitational Constant (km^3/kg/s^2)
m1 	= 5.974E+024			# Earth mass (Kg)
R 	= 6378.14				# Earth radius (Km)
m2 	= 1000					# Satellite mass (Kg)
mu 	= -G*(m1 + m2)			# mu - gravitational parameter (km^3/s^2)

# ----------- Numerical Integration -----------
r0 	= [8000.0, 0.0, 6000.0]	# [x-coordinate, y-coordinate, z-coordinate]
v0 	= [ 0.0,  7.0, 0.0 ]	# [x-velocity, y-velocity, z-velocity ]
trv0 = array(r0 + v0) 		# [x, y, z,vx, vy, vz]
dim = len(trv0)				# array size

# ----------- Simulation time params -----------
t0 	= 0						# initial time
tf 	= 3600 * 4.2			# 4.2 hours in seconds.
t  	= 0						# variable used in step
n  	= 10000
dt 	= (tf - t0)/n			# time slice
# T        = 5
# dt       = 0.0003
# accuracy = 0.0001

# ----------- Max Min Calculation -----------
max_dist = -sys.maxsize; min_dist = sys.maxsize;
velo_at_max = 0; velo_at_min = 0;
angle_at_max = 0; angle_at_min = 0;

# ============ MAIN PROGRAM BODY =========================
def testMethod( trv0, dt, fT, n):
    print " "
    F = zeros((6,n));
    out = integrate(trv0, dt, fT, n);
    result()

if __name__ == "__main__":
	#_test()
	testMethod( trv0, dt, tf, n )
